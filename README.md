# Sistema de Encuestas Dinámicas
El siguiente Proyecto es un Sistema Backend dinámico encargado de crear encuestas y registrar las respuestas de estas.
###Tecnologías
* Django Framework
* Django REST-Framework
* django-nested-inline
* modulo Collections Python

## Instalacion

* Crea un Entorno Virtual para la intalacion de los paquetes necesarios con el siguiente comando
```bash
    python -m venv virtual-env
```

* Clona el proyecto desde el siguiente [Enlace](https://gitlab.com/pportugal190895/encuestas-paul-portugal)
```bash
    https://gitlab.com/pportugal190895/encuestas-paul-portugal
```

* Si tu sistema operativo es Windows ubicate en el fichero **virtual-env\Scripts** y ejecuta el siguiente comando para activar el entorno virtual, en Caso de ser Linux la ruta es **virtual-env\bin\activate**
```bash
    activate.bat  (windows)
    source activate.bat  (Linux)
``` 

* Una vez activado el Entorno Virtual es necesario instalar los paquetes, para esto regresa a la ubicacion del proyecto 
y ejecuta el siguiente comando
```bash
    pip install -r requirements.txt
```


* La estructura de archivos deberia tener la siguiente forma 
```
    ├── virtual-env
    │   ├── Include
    │   ├── Lib
    │   ├── Scripts(Windows)/Source(Linux)
    │   ├── Encuestas Proyect
    │   │   ├── ...
    │   │   ├── encuestas
    │   │   ├── manage.py
    │   │   ├── requirements.txt
    │   │   └── ...
    │   └── ...
    └──
```


* Ahora es necesario Migrar los modelos de la siguiente manera
```bash
    python manage.py migrate
```

* Creamos un superusuario con el siguiente comando
```
    python manage.py createsuperuser
```

* Para finalizar iniciaremos el servidor y ejecutaremos el proyecto
```bash
    python manage.py runserver
```


## Sistema de Encuestas (Backend)
### Crear Encuestas 
* Dirigirse a la siguiente direccion en el navegador localhost:8000/admin 
* Loguearse con el Super Usuario.
* Crear una encuesta en el administrador de Django


### Endpoint: Detalle de encuesta
* Hacer Get a la siguiente direccion http://localhost:8000/rest/encuestas/detail
para obtener el listado de las encuestas.
* Añade un id al url para obtener el detalle de una encuesta en especifico tal que asi: http://localhost:8000/rest/encuestas/detail/1/


### Endpoint: Crear Respuesta
 
* Hacer POST a la siguiente direccion http://localhost:8000/rest/encuestas/answer/survey/ enviar en el body de entrada el json de las respuestas.
* Dicho JSON debe tener la siguiente estructura:
```
[
    {
        "question": 5,
        "answer": "Esta es una respuesta Textual",
        "alternative": null,
        "order_alternative": null
    },
    {
        "question": 7,
        "answer": null, //Esta es una respuesta de eleccion.
        "alternative": 12,
        "order_alternative": null
    },
    {
        "question": 10,
        "answer": null, //Esta es una respuesta de ordenamiento.
        "alternative": null,
        "order_alternative": [
            {
                "alternative":26, 
                "top":2
            },
            {
                "alternative":27, 
                "top":1
            },
            {
                "alternative":28, 
                "top":4
            },
            {
                "alternative":29, 
                "top":3
            }
        ]
    }
]
```
### Endpoint: Reportes
* Hacer GET a la siguiente direccion: http://localhost:8000/rest/encuestas/survey/report/ID

