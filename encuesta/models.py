from django.db import models


class Survey(models.Model):
    """
    Modelo que guarda los datos de una encuesta.

    :cvar name: Variable tipo Char que guarda el nonbre de la encuesta.
    :cvar date: Variable tipo Date que guarda la fecha de la encuesta.
    :cvar description: Variable tipo Text que guarda la descripcion de la encuesta.
    """
    name = models.CharField(max_length=64)
    date = models.DateField()
    description = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = "Encuesta"
        verbose_name_plural = "Encuestas"

    def __str__(self):
        return f"{self.name}"


class Question(models.Model):
    """
    Modelo que guarda los datos de una Pregunta.

    :cvar question: Variable tipo Char que guarda la pregunta.
    :cvar title: Variable tipo Char que guarda el titulo de una pregunta.
    :cvar description: Variable Char tipo que guarda la descripcion de una
        pregunta.
    :cvar survey: Variable tipo ForeingKey que guarda la relacion con el modelo
        Encuesta.
    """
    question = models.CharField(max_length=64)
    title = models.CharField(max_length=64, null=True, blank=True)
    description = models.CharField(max_length=64, null=True, blank=True)
    survey = models.ForeignKey('encuesta.Survey', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Pregunta"
        verbose_name_plural = "Preguntas"

    def __str__(self):
        return f'{self.question}'


class AlternativeQuestion(Question):
    """
    Modelo heredado que guarda una Pregunta del tipo Alternativo.
    """

    class Meta:
        verbose_name = "Pregunta con alternativa"
        verbose_name_plural = "Preguntas con alternativa"

    def __str__(self):
        return f"{self.question}"


class OrderingQuestion(Question):
    """
    Modelo heredado que guarda una Pregunta del tipo Ordenatorio.
    """

    class Meta:
        verbose_name = "Pregunta de ordenamiento"
        verbose_name_plural = "Preguntas de ordenamiento"

    def __str__(self):
        return f"{self.question}"


class Alternative(models.Model):
    """
    Modelo que guarda los datos de una alternativa.

    :cvar question: Variable de tipo ForeignKey que guarda la relacion con la
        Pregunta.
    :cvar alternative: Variable de tipo Char que guarda la alternativa.
    """
    question = models.ForeignKey('encuesta.Question', on_delete=models.CASCADE)
    alternative = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.alternative}"


class Answer(models.Model):
    """
    Modelo que guard los datos de una Respuesta.

    :cvar question: Variable del tipo ForeignKey que guarda la relacion con el
        modelo Pregunta.
    :cvar answer: Variable del tipo Text que guarda la respuesta tipo Textual
    :cvar alternative: Variable del tipo ForeignKey que guarda la respuesta
        elegida como relacion al modelo Alternativa.
    :cvar order_alternative: Variable del tipo Integer que guarda el rank de una
        alternativa para ordenarlo.
    """
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answer = models.TextField(max_length=128, null=True, blank=True)
    alternative = models.ForeignKey(
        'Alternative',
        null=True, blank=True,
        on_delete=models.CASCADE
    )
    order_alternative = models.IntegerField(null=True, blank=True)

    def __str__(self):
        if self.answer:
            return f'{self.answer}'
        if self.alternative:
            return f'{self.alternative}'
        return f'{self.order_alternative}'

