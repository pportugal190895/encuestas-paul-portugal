from django.contrib import admin
from nested_inline.admin import NestedModelAdmin, NestedTabularInline

from encuesta.models import (
    Question,
    AlternativeQuestion,
    OrderingQuestion,
    Alternative,
    Survey, Answer)


class AlternativeNestedStackedInline(NestedTabularInline):
    """
    Admin de alternativa personalizado para que este anidado a una pregunta.
    """
    model = Alternative
    extra = 1
    fk_name = 'question'


class QuestionNestedStackedInline(NestedTabularInline):
    """
    Admin de la pregunta personalizado para que este anidado a una Encuesta.
    """
    model = Question
    extra = 1
    fk_name = 'survey'


class AlternativeQuestionNestedStackedInline(NestedTabularInline):
    """
    Admin de la pregunta tipo Alternativa personalizado para que este anidado a
    una Encuesta.
    """
    model = AlternativeQuestion
    extra = 1
    fk_name = 'survey'
    inlines = (AlternativeNestedStackedInline,)


class OrderingNestedStackedInline(NestedTabularInline):
    """
    Admin de la pregunta tipo Ordenatoria personalizado para que este anidado a
    una Encuesta.
    """
    model = OrderingQuestion
    extra = 1
    fk_name = 'survey'
    inlines = (AlternativeNestedStackedInline,)


class SurveyAdmin(NestedModelAdmin):
    """
    Admin de la Encuesta personalizado para anidar las preguntas y alternativas.
    """
    list_display = ('name', 'date', 'description')
    inlines = (
        QuestionNestedStackedInline,
        AlternativeQuestionNestedStackedInline,
        OrderingNestedStackedInline,
    )


class AnswerAdmin(admin.ModelAdmin):
    """
    Admin de las respuestas personalizado para que no se peudan modificar los
    datos.
    """
    readonly_fields = (
        'question', 'answer', 'alternative', 'order_alternative',
    )
    list_display = ('question', 'survey')

    def survey(self, obj):
        return obj.question.survey


admin.site.register(Survey, SurveyAdmin)
admin.site.register(Answer, AnswerAdmin)
