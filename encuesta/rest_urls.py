from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from encuesta.rest_views import (
    DetailSurveyViewSet,
    AnswerSurveyViewSet,
    SurveyReportViewSet,
)

router = DefaultRouter()

router.register(r'detail', DetailSurveyViewSet)
router.register(r'answer/survey', AnswerSurveyViewSet, basename="answer_survey")
router.register(r'survey/report', SurveyReportViewSet, basename="survey_report")


urlpatterns = [
    url(r'encuestas/', include(router.urls))
]
