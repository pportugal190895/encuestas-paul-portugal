from rest_framework import serializers

from encuesta.models import Survey, Alternative, Question, Answer


class AlternativeSerializer(serializers.ModelSerializer):
    """
    Serializer generico usado para serializar un modelo Alternativa
    """
    class Meta:
        model = Alternative
        fields = ('alternative', )


class QuestionSerializer(serializers.ModelSerializer):
    """
    Serializer generico usado para serializar un modelo Pregunta y anidar sus
    alternativas.
    """
    alternatives = serializers.SerializerMethodField('get_alternatives')

    def get_alternatives(self, obj):
        alternatives = Alternative.objects.filter(question=obj)
        if alternatives:
            serializer = AlternativeSerializer(alternatives, many=True)
            return serializer.data
        return None

    class Meta:
        model = Question
        fields = ('question', 'alternatives',)


class DetailSurveySerializer(serializers.ModelSerializer):
    """
    Serializer generico usado para serializar un modelo Encuesta y anidar sus
    preguntas.
    """
    questions = serializers.SerializerMethodField('get_questions')

    def get_questions(self, obj):
        questions = Question.objects.filter(survey=obj)
        if questions:
            serializer = QuestionSerializer(questions, many=True)
            return serializer.data
        return None

    class Meta:
        model = Survey
        fields = ('name', 'date', 'description', 'questions',)
