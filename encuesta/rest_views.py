import json
from collections import Counter

from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.response import Response

from encuesta.models import (
    Survey,
    Question,
    Answer,
    Alternative,
    AlternativeQuestion,
    OrderingQuestion
)
from encuesta.serializers import DetailSurveySerializer


class DetailSurveyViewSet(viewsets.ModelViewSet):
    """
    ViewSet Generico del tipo ModelViewSet usado para vizualizar el detalle de
    una encuesta:
    GET: http://localhost:8000/rest/encuestas/detail        Listado de encuestas
    GET: http://localhost:8000/rest/encuestas/detail/ID     Detalle de encuesta
    """
    serializer_class = DetailSurveySerializer
    queryset = Survey.objects.all()


class AnswerSurveyViewSet(viewsets.ViewSet):
    """
    ViewSet usado para crear una respuesta:
    POST: http://localhost:8000/rest/encuestas/answer/survey/
    con el JSON en el cuerpo tal como se especifica en el README del proyecto:
    """
    def create(self, request):
        """
        Metodo Post
        :param request: Metadata de la solicitud.
        :return: Metodo Response para retornar un Status
        """
        answers = request.data
        for answer in answers:
            # Forloop para recorrer las respuestas
            question = Question.objects.get(id=answer['question'])
            if answer['answer']:
                # Si es del tipo textual se agrega la respuesta
                answer = Answer.objects.create(
                    question=question, answer=answer['answer']).save()
            elif answer['alternative']:
                # Si es del tipo alternativa se agrega la respuesta
                alternative = Alternative.objects.get(id=answer['alternative'])
                answer = Answer.objects.create(
                    question=question, alternative=alternative).save()
            elif answer['order_alternative']:
                # Si es del tipo ordenatorio se agrega la respuesta
                for order_answer in answer['order_alternative']:
                    # Forloop usado para añadir un rank a la alternativa
                    alternative = Alternative.objects.get(id=order_answer['alternative'])
                    answer = Answer.objects.create(
                        question=question,
                        alternative=alternative,
                        order_alternative=order_answer['top']
                    ).save()
            else:
                return Response({'Mensaje': "Formulario invalido"}, status=400)
        return Response({'Mensaje': "Se registro tu respuesta"}, status=200)


class SurveyReportViewSet(viewsets.ViewSet):
    """
    ViewSet usado para devolver el reporte de una encuesta:
    GET: http://localhost:8000/rest/encuestas/survey/report/ID
    """
    def retrieve(self, request, pk=None):
        """
        Metodo get usado para devolver los datos en formato JSON.
        :param request: Metadata de la solicitud.
        :param pk: parametro de tipo entero usado apra obtener la encuesta de la
            cual se quiere el reporte
        :return: Metodo Response usado para devolver los datos en formato JSON
            y un Status.
        """
        survey = Survey.objects.get(id=pk)
        answers_report = {}
        answers_report["text  questions report"] = self.text_report(survey)
        answers_report["alternatives questions report"] = self.alternative_report(survey)
        answers_report["ordering questions report"] = self.ordering_report(survey)
        data = answers_report
        return JsonResponse(data, safe=False)

    def text_report(self, survey):
        """
        Metodo usado para construir el reporte de las preguntas del tipo textual
        :param survey: objeto del tipo Encuesta
        :return: dict con los datos.
        """
        text_answers_report = {}
        for question in Question.objects.filter(survey=survey, alternative=None):
            text_anwers = Answer.objects.filter(
                question__alternative=None, question=question
            )

            universe_words = []
            for answer in text_anwers:
                for word in answer.answer.split():
                    universe_words.append(word.lower())

            counter = Counter(universe_words)
            most_occur = counter.most_common(50)
            text_report = []
            for word in most_occur:
                percentage = word[1] * 100 / len(universe_words)
                text_report.append(
                    {'word': word[0], 'recurrencia': word[1], '%': percentage}
                )
            text_answers_report[question.id] = text_report
        return text_answers_report

    def alternative_report(self, survey):
        """
         Metodo usado para construir el reporte de las preguntas del tipo alternativa
         :param survey: objeto del tipo Encuesta
         :return: dict con los datos.
         """
        alternative_answers_report = {}

        for question in AlternativeQuestion.objects.filter(survey=survey):
            alternatives = Alternative.objects.filter(question=question)
            alternative_report = []

            for alternative in alternatives:
                alternative_anwers = Answer.objects.filter(
                    alternative=alternative
                ).count()
                answers = Answer.objects.filter(question=question).count()
                percentage = alternative_anwers*100/answers
                alternative_report.append(
                    {
                        'alternativa': alternative.alternative,
                        'porcentaje': percentage,
                        'total de respuestas': alternative_anwers
                    }
                )
            alternative_answers_report[question.id] = alternative_report

        return alternative_answers_report

    def ordering_report(self, survey):
        """
        Metodo usado para construir el reporte de las preguntas del tipo ordenatorio
        :param survey: objeto del tipo Encuesta
        :return: dict con los datos.
        """
        ordering_answers_report = {}

        for question in OrderingQuestion.objects.filter(survey=survey):
            alternatives = Alternative.objects.filter(question=question)
            alternative_report = []

            for alternative in alternatives:
                alternative_anwers = Answer.objects.filter(
                    alternative=alternative
                )
                ranks = [answer.order_alternative for answer in alternative_anwers]
                counter = Counter(ranks)
                most_ranked = counter.most_common(1)
                percentage = most_ranked[0][1] * 100 / alternative_anwers.count()
                alternative_report.append(
                    {
                        'alternativa': alternative.alternative,
                        'rank': most_ranked[0][0],
                        'porcentaje': percentage
                    }
                )

            ordering_answers_report[question.id] = alternative_report

        return ordering_answers_report
